import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import api from './api/api'
import $ from 'jquery'
import SIdentify from './views/Identify.vue'
import 'element-ui/lib/theme-chalk/index.css'
import '@/assets/css/common.css'
import '@/assets/static/iconfont/font/ali-font.css'
import '@/assets/static/iconfont/icon/iconfont.css'
import { Picker, Popup, Uploader, Field, NumberKeyboard, SwipeCell, Button, Checkbox, CheckboxGroup, SubmitBar, Stepper, NavBar, Skeleton, ImagePreview} from 'vant'
Vue.use(Popup)
Vue.use(Picker)
Vue.use(NumberKeyboard)
Vue.use(Field)
Vue.use(SIdentify)
Vue.use(ElementUI)
Vue.use(Uploader).use(SwipeCell).use(Popup).use(Button).use(Checkbox).use(CheckboxGroup).use(SubmitBar).use(Stepper).use(Skeleton).use(NavBar).use(ImagePreview)
Vue.prototype.$ = $
Vue.prototype.$api = api
Vue.config.productionTip = false
Vue.prototype.$connectSocket = function (host) {
  if (typeof WebSocket === "undefined") {
    alert("您的浏览器不支持socket");
  } else {
    window.webSocket = new WebSocket(host)
    // 监听socket连接
    window.webSocket.onopen = function () {
      console.info('[Socket Local] 连接成功');
    }
    // 监听socket错误信息
    window.webSocket.onerror = function () {
      console.error("[Socket Local] 连接错误");
    }
    // 监听socket消息
    window.webSocket.onmessage = function (msg) {
      var MesList = JSON.parse(msg.data)
      console.log(MesList)
    }
  }
}
new Vue({
  router,
  store,
  render: h => h(App),
  mounted () {
    store.dispatch('userInfo/tryFetchUserInfo').then()
  }
}).$mount('#app')
