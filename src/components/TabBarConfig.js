import index from '@/assets/public/shouye_icon.png'
import indexChosen from '@/assets/public/shouye_icon_chosen.png'
import discover from '@/assets/public/faxian_icon.png'
import discoverChosen from '@/assets/public/faxian_icon_chosen.png'
import message from '@/assets/public/xiaoxi_icon.png'
import messageChosen from '@/assets/public/xiaoxi_icon_chosen.png'
import setting from '@/assets/public/shezhi_icon.png'
import settingChosen from '@/assets/public/shezhi_icon_chosen.png'

import Index from '@/views/Index'
import Message from '@/views/Message'
import Discover from '@/views/Discover'
import Setting from '@/views/Setting'

let tabBarConfig = [
  {
    path: '/index',
    name: 'index',
    component: Index,
    icon: index,
    iconChosen: indexChosen,
    label: '首 页'
  },
  {
    path: '/discover',
    name: 'discover',
    component: Discover,
    icon: discover,
    iconChosen: discoverChosen,
    label: '发 现'
  },
  {
    path: '/message',
    name: 'message',
    component: Message,
    icon: message,
    iconChosen: messageChosen,
    label: '消 息'
  },
  {
    path: '/setting',
    name: 'setting',
    component: Setting,
    icon: setting,
    iconChosen: settingChosen,
    label: '设 置'
  }
]

export default tabBarConfig
