import httpRequest from '@/api/httpRequest'
let axios = httpRequest.axios

// example from caocao
// user account services
let register = (userName, password,phone) => {
  return axios.post('/user/api/user/register', {
    'userName': userName,
    'password': password,
    'phone': phone

  })
}

let login = (userName, password) => {
  return axios.post('/user/api/user/login', {
    'userName': userName,
    'password': password
  })
}

let wxlogin = code => {
  return axios.get(`/user/api/user/wx?code=` + code)
}

let getWxConfig = _ => {
  return axios.get('/message/api/wechat/credentials')
}

let logout = _ => {
  return axios.post('/user/api/user/logout', {

  })
}

// user info
let userInfo = userId => {
  return axios.get(`/user/api/user/${userId}/info`)
}

let myInfo = _ => {
  return axios.get('/user/api/user/my/info')
}

let putMyInfo = (name, profilePhotoUrl, sexual, university, college, major, introduction, age, phoneNumber, education) => {
  return axios.put('/user/api/user/my/info', {
    name, profilePhotoUrl, sexual, university, college, major, introduction, age, phoneNumber, education
  })
}

// tags controller
let addTag = (name, description) => {
  return axios.post('/user/api/tags', {
    name, description
  })
}

let putTag = (tagId, newName, newDescription) => {
  return axios.put('/user/api/tags', {
    id: tagId,
    name: newName,
    description: newDescription
  })
}

let deleteTag = tagId => {
  return axios.delete('/user/api/tags', {
    id: tagId
  })
}

let getTag = tagId => {
  return axios.get(`/user/api/tags/${tagId}`)
}

let getTags = _ => {
  return axios.get(`/user/api/tags`)
}

let getUserTags = userId => {
  return axios.get(`/user/api/user/${userId}/tags`)
}

let getMyTags = _ => {
  return axios.get('/user/api/user/my/tags')
}

let addMyTagById = tagId => {
  return axios.post('/user/api/user/tags', {
    id: tagId
  })
}

let addMyTagByName = tagName => {
  return axios.post('/user/api/user/tags', {
    name: tagName
  })
}

let deleteMyTagById = tagId => {
  return axios.delete('/user/api/user/tags', {
    data: {
      id: tagId
    }
  })
}

let deleteMyTagByName = tagName => {
  return axios.delete('/user/api/user/tags', {
    data: {
      name: tagName
    }
  })
}

// follow controller
let getUserFollowerNum = id => {
  return axios.get(`/user/api/user/follow/${id}`)
}

let follow = userId => {
  return axios.post(`/user/api/user/follow/${userId}`)
}

let myFollow = _ => {
  return axios.get(`/user/api/user/follow/my`)
}

let unfollow = userId => {
  return axios.delete(`/user/api/user/follow/${userId}`)
}

let getFans = userId => {
  return axios.get(`/user/api/user/follow/${userId}/followers`)
}

let getFollowings = userId => {
  return axios.get(`/user/api/user/follow/${userId}/followings`)
}

let getFollowRelation = (userId, targetId) => {
  return axios.get(`/user/api/user/follow/relation?userId=${userId}&targetId=${targetId}`)
}

export default {
  register,
  login,
  logout,
  userInfo,
  myInfo,
  putMyInfo,
  addTag,
  putTag,
  deleteTag,
  getTag,
  getTags,
  getUserTags,
  getMyTags,
  addMyTagById,
  addMyTagByName,
  deleteMyTagById,
  deleteMyTagByName,
  getUserFollowerNum,
  follow,
  unfollow,
  getFans,
  getFollowings,
  myFollow,
  wxlogin,
  getWxConfig,
  getFollowRelation
}
