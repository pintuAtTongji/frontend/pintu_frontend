import httpRequest from '@/api/httpRequest'
let axios = httpRequest.axios

let getMyFavoriteBoards = userId => {
  return axios.get(`/board/api/user/${userId}/board/collect`)
}

let getMyBoards = userId => {
  return axios.get(`/board/api/user/${userId}/board/create`)
}

let getMyAttendBoards = userId => {
  return axios.get(`/board/api/user/${userId}/board/attend`)
}

let getNews = page => {
  return axios.get(`/board/api/news?page=${page}`)
}

let getBoardDetail = boardId => {
  return axios.get(`/board/api/board/${boardId}`)
}

let getBoardAttenders = boardId => {
  return axios.get(`/board/api/board/${boardId}/attender`)
}

let collect = (boardId, userId) => {
  return axios.post(`/board/api/collect`, {
    'boardId': boardId,
    'userId': userId
  })
}

let uncollect = (boardId, userId) => {
  return axios.post(`/board/api/uncollect`, {
    'boardId': boardId,
    'userId': userId
  })
}

let attend = (boardId, userId) => {
  return axios.post(`/board/api/attend`, {
    'boardId': boardId,
    'userId': userId
  })
}

let exit = (boardId, userId) => {
  return axios.post(`/board/api/exit`, {
    'boardId': boardId,
    'userId': userId
  })
}

let postNewBoard = (boardContent, relatedImage, boardTitle, userCount, userId) => {
  return axios.post('/board/api/board', {
    'content': boardContent,
    'relatedImage': relatedImage,
    'title': boardTitle,
    'userCount': userCount,
    'userId': userId
  })
}

export default {
  getMyFavoriteBoards,
  getMyBoards,
  getMyAttendBoards,
  getNews,
  getBoardDetail,
  getBoardAttenders,
  collect,
  uncollect,
  attend,
  exit,
  postNewBoard
}
