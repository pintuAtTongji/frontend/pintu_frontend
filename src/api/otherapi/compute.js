import httpRequest from '@/api/httpRequest'
let axios = httpRequest.axios

let discover = userId => {
  return axios.get(`/compute/api/discover/${userId}`)
}

let search = keyword => {
  return axios.post(`/compute/api/search`, {
    key: keyword
  })
}

export default {
  discover, search
}
