import httpRequest from './httpRequest'
let axios = httpRequest.axios;

let helloUser = function(){
    return axios.get('/user/api/hello');
}
let helloBoard = function(){
    return axios.get('/board/api/hello');
}
let helloCompute = function() {
  return axios.get('/compute/api/hello');
}

import userApi from './otherapi/user'
import fileApi from './otherapi/file'
import boardApi from './otherapi/board'
import computeApi from '@/api/otherapi/compute'

export default {
    helloUser,
    helloBoard,
    helloCompute,
  ...userApi,
  ...fileApi,
  ...boardApi,
  ...computeApi
}
