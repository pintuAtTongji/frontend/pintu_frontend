/* httpRequest.js
 * Copyright @author: chaoszh
 * Time 2019-10-17
 */

import axios from 'axios'

// let baseURL = 'http://47.101.53.252:10000/';
let baseURL = '/api';

let axiosInstance = axios.create({
  baseURL,
  withCredentials: true
})

axiosInstance.interceptors.request.use(
  // 在请求时log请求基本信息
  requestConfig => {
    console.log(requestConfig.method.toUpperCase(), requestConfig.url, requestConfig.data)
    return requestConfig
  },
  error => {
    console.log(error)
    return Promise.reject(error)
}
)

axiosInstance.interceptors.response.use(
    // 在返回的时候log请求的基本信息
    response => {
        console.log(response)
        return response
    },
    error => {
        console.log(error)
        Promise.reject(error)
    }
)

export default {
  axios: axiosInstance
}
