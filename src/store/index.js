import Vue from 'vue'
import Vuex from 'vuex'
import api from '@/api/api'

Vue.use(Vuex)

const tabBar = {
  namespaced: true,
  state: {
    index: 0,
    newMessage: true
  },
  mutations: {
    switchTab (state, payload) {
      state.index = payload || 0
    },
    setNewMessage (state) {
      state.newMessage = true
    },
    unsetNewMessage (state) {
      state.newMessage = false
    }
  }
}

const userInfo = {
  namespaced: true,
  state: {
    detail: null
  },
  mutations: {
    setUserInfo (state, payload) {
      state.detail = payload
    }
  },
  actions: {
    tryFetchUserInfo (context) {
      api.myInfo().then(res => {
        if (res.data.code === 200) {
          context.commit('setUserInfo', res.data.data)
        }
      })
    }
  },
  getters: {
    logged: state => state.detail != null
  }
}

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    tabBar,
    userInfo
  }
})
