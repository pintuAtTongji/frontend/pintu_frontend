import Vue from 'vue'
import VueRouter from 'vue-router'
import tabBarConfig from '@/components/TabBarConfig'
import TabBarView from '@/components/TabBarView'
import PersonInfoToItself from '@/components/setting/PersonalInfo_toItself'
import First from '@/views/First'
import Login from '@/views/Login'
import Upload from '@/views/Upload'
import Register from '@/views/Register'
import UserInfo from '@/views/UserInfo'
import ApiExample from '@/views/ApiExample'
import BoardDetail from '@/views/BoardDetail'
import Test from '@/views/temp/Test'
import PersonInfo from '@/views/temp/PersonInfo'
import PersonFavoriteBoards from '@/components/setting/PersonFavoriteBoards'
import PersonParticipateBoards from '@/components/setting/PersonParticipateBoards'
import PersonSponsorialBoards from '@/components/setting/PersonSponsorialBoards'
import MessageClick from '@/views/MessageClick'
import FollowList from '@/views/FollowList'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'tabBarViews',
    redirect: '/first',
    component: TabBarView,
    children: tabBarConfig.map(item => {
      return {
        path: item.path, name: item.name, component: item.component
      }
    })
  },
  {
    path: '/boardDetail',
    component: BoardDetail
  },
  {
    path: '/personInfo_toItself',
    name: 'personInfo_toItself',
    component: PersonInfoToItself
  },
  {
    path: '/first',
    name: 'first',
    component: First
  },
  {
    path: '/upload',
    name: 'upload',
    component: Upload
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  },
  {
    path: '/example',
    name: 'apiExample',
    component: ApiExample
  },
  {
    path: '/favoriteBoards',
    name: 'favoriteBoards',
    component: PersonFavoriteBoards
  },
  {
    path: '/participateBoards',
    name: 'participateBoards',
    component: PersonParticipateBoards
  },
  {
    path: '/sponsorialBoards',
    name: 'sponsorialBoards',
    component: PersonSponsorialBoards
  },
  {
    path: '/test',
    name: 'Test',
    component: Test
  },
  {
    path: '/test2',
    name: 'PersonInfo',
    component: PersonInfo
  },
  {
    path: '/user',
    name: 'user',
    component: UserInfo
  },
  {
    path: '/messageContent/:id',
    name: 'MessageClick',
    component: MessageClick
  },
  {
    path: '/followList',
    name: 'followList',
    component: FollowList
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
