const path = require('path')
function resolve (dir) {
  return path.join(__dirname, dir)
}

const webpack = require('webpack')
module.exports = {
  lintOnSave: false,
  chainWebpack: config => {
    config.plugin('provide').use(webpack.ProvidePlugin, [{
      $: 'jquery',
      jquery: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    }])
    // 路径配置
    config.resolve.alias
      .set('@', resolve('./src'))
    //   .set('assets', resolve('./src/assets/'))
  },
  devServer: {
    proxy: {
      '/api': {
        target: 'http://47.101.53.252:10000/', // 对应自己的接口
        changeOrigin: true,
        // ws: true,
        pathRewrite: {
          '^/api': ''
        }
      }
    }
  },
  publicPath: './'
}
