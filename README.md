# pintu_frontend
这是拼图项目的前端部分。基于Vue框架编写。

## 项目依赖
项目使用npm管理项目依赖和构建。在项目底层目录运行` npm install ` 来安装依赖。

主要依赖包括[vue](https://cn.vuejs.org/index.html), [vue-router](https://router.vuejs.org/zh/), [vuex](https://vuex.vuejs.org/zh/), [barbel](https://babeljs.io/), [eslint](https://eslint.org/), [less](http://lesscss.org/), [axios](https://github.com/axios/axios)等。

## 项目运行

建议使用WebStorm来管理和运行项目。

### 启动hot-reload的调试平台

`npm run serve`

### 打包文件
`npm rum build`

### 运行代码风格检查和自动修正
`npm run lint`

## 项目目录结构
-  public

    存放静态文件和初始页面模版。由于使用webpack管理构建，建议所有资源文件都存放在src/assets(见下文)并尽量不修改这个目录。
    
-  src

    存放项目直接源码的位置。

    -  assets

        存放运行时的资源（主要为图片）。被多个页面引用的文件请放置在public文件夹。其他文件请自行创建对应页面名称的文件夹放置。

        另注：在项目中引用这些文件的方式：使用`import 变量名 from '@/assets/文件路径'`即可在js定义一个变量取得文件打包后的url，可直接绑定于img的src等位置。在css中引用时请直接使用相对路径即可。
        
    -  components
    
        存放非页面的、自定义的Vue组件（或许还有对应的配置）。作为一个单页面应用，应用中对页面和组件的区分并不明确。大家可自行处理。
    
    -  router
    
        存放vue-router的配置文件。请大家自行学习这一框架的用法。对于主页的4个页面，相关配置已经写入tabBar的配置文件并在此载入。建议大部分页面不进行路径嵌套。请不要忘记创建新的页面后在此引入component并配置router路径。
    
    -  store
    
        存放vuex的配置文件。请大家自行学习这一组件。目前计划使用该组件管理全局的tabBar选中情况（避免因为路由跳转引发的bug）和未来加入的用户登录信息。如果需要进行复杂数据的跨页面共享，可以在此文件中定义新的module并自行使用。(简单数据的单向传递请使用vue-router的param参数)
    
    -  views
    
        存放最主要的页面文件。由于页面不多，并不在此目录设置复杂的目录结构。所有的页面请使用清晰的命名并直接放置在目录内即可。
    
    -  App.vue main.js
    
        初始化文件，忽略即可。
    
-  其他文件

    大多数为项目层的配置文件。由于修改此处文件将对全局产生影响，请谨慎修改（添加依赖而造成的package.json修改不在此列）



## 最后

项目使用git管理。建议在各自分支上开发并定期merge。如有任何更多关于项目、框架、插件等的技术细节，请尽情戳我或者自行查阅相关的文档。Happy coding!



